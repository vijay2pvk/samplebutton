//
//  InitialViewControllerTests.swift
//  SampleButtonTests
//
//  Created by Vijaya Kumar on 11/10/18.
//  Copyright © 2018 Vijaya Kumar. All rights reserved.
//

import XCTest

@testable import SampleButton

class InitialViewControllerTests: XCTestCase {

    var sut: InitialViewController!
    
    override func setUp() {
        super.setUp()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: "InitialViewController")
        self.sut = (viewController as! InitialViewController)
        
        self.sut.loadViewIfNeeded()
        
    }
    
    override func tearDown() {
        sut = nil
        
        super.tearDown()
    }

    func testTitle_ToggleEnablingButton() {
        XCTAssertNotNil(sut.toggleEnablingButton)
        
        XCTAssertEqual(sut.toggleEnablingButton.titleLabel?.text, "Enable")
        XCTAssertEqual(sut.toggleEnablingButton.isEnabled, true)
    }
    
    func testIsEnabled_RoundCornerButton() {
        XCTAssertNotNil(sut.roundCornerButton)
        XCTAssertEqual(sut.roundCornerButton.isEnabled, false)
    }
    
    func testIsEnabled_MyButton() {
        XCTAssertNotNil(sut.myButton)
        XCTAssertEqual(sut.myButton.isEnabled, false)
    }
    
    func testAction_ToggleEnablingButton() {
        sut.toggleEnablingButtonPressed(sut.toggleEnablingButton)
        XCTAssertEqual(sut.roundCornerButton.isEnabled, true)
        XCTAssertEqual(sut.toggleEnablingButton.titleLabel?.text, "Disable")
        
    }
}
