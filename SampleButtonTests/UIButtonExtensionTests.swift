import XCTest

@testable import SampleButton

class UIButtonExtensionTests: XCTestCase {

    func testUIButton_Sets1by1BackgroundImageUsingColour() {
        let sut = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        sut.setBackgroundColor(UIColor(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)

        let backgroundImage = sut.backgroundImage(for: .normal)

        XCTAssertEqual(backgroundImage?.size.width, 1.0)
        XCTAssertEqual(backgroundImage?.size.height, 1.0)
    }
}
