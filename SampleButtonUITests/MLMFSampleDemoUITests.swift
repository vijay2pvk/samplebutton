//
//  SampleButtonUITests.swift
//  SampleButtonUITests
//
//  Created by Vijaya Kumar on 9/10/18.
//  Copyright © 2018 Vijaya Kumar. All rights reserved.
//

import XCTest

class SampleButtonUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUp() {
        super.setUp()
        
        continueAfterFailure = false
        
        app = XCUIApplication()
        app.launchArguments += ["UI-Testing"]
        app.launch()
    }

    func testEnableButton() {

        let enableButton = app.buttons["Enable"]
        XCTAssertTrue(enableButton.exists)

        enableButton.tap()
        XCTAssertNotNil(enableButton)
        XCTAssertFalse(enableButton.exists)
        
        let disableButton = app.buttons["Disable"]
        XCTAssertTrue(disableButton.exists)
    }

}
