import UIKit

extension UIColor {
    /**
     Creates an UIColor from HEX String in "#363636" format

     - parameter hexString: HEX String in "#363636" format
     - returns: UIColor from HexString
     */
    convenience init(hexString: String) {

        let hexString: String = (hexString as NSString).trimmingCharacters(in: .whitespacesAndNewlines)
        let scanner = Scanner(string: hexString as String)

        if hexString.hasPrefix("#") {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)

        let mask = 0x000000FF

        let cRed = Int(color >> 16) & mask
        let cGreen = Int(color >> 8) & mask
        let cBlue = Int(color) & mask

        let redColor = CGFloat(cRed) / 255.0
        let greenColor = CGFloat(cGreen) / 255.0
        let blueColor = CGFloat(cBlue) / 255.0

        self.init(red: redColor, green: greenColor, blue: blueColor, alpha: 1)
    }

}
