import UIKit

class CustomButton: UIButton {

    // MARK: - Properties
    var shadowLayer: CAShapeLayer!

    var customButtonColor: UIColor = Color.customDarkBlue.value
    var customTitleColor: UIColor = .white
    var customDisabledTitleColor: UIColor = .lightGray

    @IBInspectable public var isBlueButton: Bool = true {
        willSet(isBlue) {
            if isBlue {
                self.customButtonColor = Color.customDarkBlue.value
                self.customTitleColor = .white
            } else {
                self.whiteButton()
            }
        }
    }

    @IBInspectable public var isDarkGreyButton: Bool = true {
        willSet(isDarkGrey) {
            if isDarkGrey {
                self.customButtonColor = Color.customGray.value
                self.customTitleColor = .white
            } else {
                self.whiteButton()
            }
        }
    }

    override var isEnabled: Bool {
        didSet {
            isEnabled ?
                self.setBackgroundColor(self.customButtonColor) :
                self.setBackgroundColor(Color.customDisabledButton.value)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setCornerRadiusWithShadow()
    }

    // MARK: - Methods
    override func layoutSubviews() {
        super.layoutSubviews()

        configButton()
        configTitle()
    }

    fileprivate func configButton() {
        self.clipsToBounds = false
        self.frame.size.height = 47.0
    }

    fileprivate func configTitle() {
        self.setTitleColor(customTitleColor, for: .normal)
        self.setTitleColor(customDisabledTitleColor, for: .disabled)
        self.titleLabel?.font = UIFont.systemFont(ofSize: 17).bolded
    }

    fileprivate func setCornerRadiusWithShadow() {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.setBackgroundColor(self.customButtonColor)
        self.layer.cornerRadius = 3
        
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 1
    }

    fileprivate func whiteButton() {
        self.customButtonColor = .white
        self.customTitleColor = Color.customDarkBlue.value
    }

    fileprivate func setBackgroundColor(_ color: UIColor) {
        self.backgroundColor = color;
    }
}
