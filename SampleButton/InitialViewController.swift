//
//  InitialViewController.swift
//  TestButton
//
//  Created by Vijaya Kumar on 5/10/18.
//  Copyright © 2018 Vijaya Kumar. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {
    
    @IBOutlet weak var toggleEnablingButton: UIButton!
    @IBOutlet weak var roundCornerButton: UIButton!
    
    @IBOutlet weak var myButton: UIButton!
    var isButtonEnabled : Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        isButtonEnabled = false
        
        updateButton()
        setCornerRadiusWithShadow()
    }
    
    func setCornerRadiusWithShadow() {
        myButton.layer.cornerRadius = 3
        
        myButton.layer.shadowColor = UIColor.black.cgColor
        myButton.layer.shadowOffset = CGSize(width: 0, height: 0)
        myButton.layer.shadowRadius = 5
        myButton.layer.shadowOpacity = 1
    }
    
    func updateButton() {
        toggleEnablingButton.setTitle(!isButtonEnabled ? "Enable" : "Disable", for: .normal)
        roundCornerButton.isEnabled = isButtonEnabled
        myButton.isEnabled = isButtonEnabled
        myButton.backgroundColor = isButtonEnabled ? Color.customDarkBlue.value : Color.customDisabledButton.value
    }
    
    @IBAction func toggleEnablingButtonPressed(_ sender: Any) {
        isButtonEnabled = !isButtonEnabled
        updateButton()
    }
    
    @IBAction func roundCornerButtonPressed(_ sender: Any) {
        print("roundCornerButtonPressed")
    }
    
    @IBAction func myButtonPressed(_ sender: Any) {
        print("myButtonPressed")
        
    }
    
    
}

