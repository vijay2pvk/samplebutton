import UIKit

enum Color {
    case customLightBlue
    case customDarkBlue
    case customBackgroundGray
    case customKeyboardButtonBorder
    case customDisabledTextField
    case customDisabledButton
    case customOrange
    case customNavigationBarGradientTop
    case customNavigationBarGradientBottom
    case customError
    case customSuccessGreen
    case customGray
    case zirconGray
    case alertBackground
    case customLink
    case customInfoViewBackground
    case customInfoViewBorder
    case customMacaroniAndCheese
    case customBilbaoGreen

    case custom(hexString: String, alpha: Double)

    func withAlpha(_ alpha: Double) -> UIColor {
        return self.value.withAlphaComponent(CGFloat(alpha))
    }

}

extension Color {
    var value: UIColor {
        var instanceColor = UIColor.clear

        switch self {
        case .customLightBlue:
            instanceColor = UIColor(hexString: "#00aeef")
        case .customDarkBlue:
            instanceColor = UIColor(hexString: "00467f")
        case .customBackgroundGray:
            instanceColor = UIColor(hexString: "f5f5f5")
        case .customKeyboardButtonBorder:
            instanceColor = UIColor(hexString: "c3c3c3")
        case .customDisabledTextField:
            instanceColor = UIColor(hexString: "7d7d7d")
        case .customDisabledButton:
            instanceColor = UIColor(hexString: "d2d2d2")
        case .customOrange:
            instanceColor = UIColor(hexString: "faa652")
        case .customNavigationBarGradientTop:
            instanceColor = UIColor(hexString: "00518a")
        case .customNavigationBarGradientBottom:
            instanceColor = UIColor(hexString: "0075af")
        case .customError:
            instanceColor = UIColor(hexString: "b80505")
        case .customSuccessGreen:
            instanceColor = UIColor(hexString: "a8cfad")
        case .customGray:
            instanceColor = UIColor(hexString: "4c4c4c")
        case .zirconGray:
            instanceColor = UIColor(hexString: "e0e0e0")
        case .customLink:
            instanceColor = UIColor(hexString: "1976a5")
        case .customInfoViewBackground:
            instanceColor = UIColor(hexString: "e7f1fa")
        case .customInfoViewBorder:
            instanceColor = UIColor(hexString: "00aeef")
        case .customMacaroniAndCheese:
            instanceColor = UIColor(hexString: "#fbc18b")
        case .customBilbaoGreen:
            instanceColor = UIColor(hexString: "129724")
        case .alertBackground:
            instanceColor = UIColor(hexString: "000000").withAlphaComponent(CGFloat(0.58))
        case .custom(let hexValue, let opacity):
            instanceColor = UIColor(hexString: hexValue).withAlphaComponent(CGFloat(opacity))
        }
        return instanceColor
    }
}
